@ECHO OFF
REM Change these parameters to your liking

REM Filename of the configuration file you want to use
set PROPERTIES=C:/Dropbox/IdeaProjects/cc3/gmaps_slicer/templates/dorf.xml
REM The path where your Java is installed
set JAVA="c:\Program Files\Java\jre1.8.0_66\bin\java.exe"

%JAVA% -cp ../target/gmaps_slicer-0.2-jar-with-dependencies.jar de.tkunkel.cc3.macro.img.GMapGenerator %PROPERTIES%
PAUSE