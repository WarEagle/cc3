package de.tkunkel.gmaps.slicer;

import de.tkunkel.cc3.macro.img.marker.MarkerConverter;
import de.tkunkel.gmaps.config.ConfigData;
import de.tkunkel.gmaps.config.Layer;
import de.tkunkel.gmaps.config.MarkerEntry;
import de.tkunkel.gmaps.config.MarkerGroup;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

public class HtmlUpdater {
    private final static String PATH_INDEX_TEMPLATE = "/template_index.html";
    private final static String PATH_SCROLL_IMAGE = "/scroll/complete.png";
    private ConfigData configData;
    private int nrZoomLevels;
    private Config.ImageFormat imageFormat;
    private List<Layer> layers;


    public HtmlUpdater(ConfigData configData, int nrZoomlevels, Config.ImageFormat imageFormat, List<Layer> layers) {
        this.configData = configData;
        this.nrZoomLevels = nrZoomlevels;
        this.imageFormat = imageFormat;
        this.layers = layers;
    }

    public void writeToNewFile(Path newFile) {

        String markerString = generateMarkerInformations();
        String tileLayerString = generateTileLayerString();

        try (InputStream in = getClass().getResourceAsStream(PATH_INDEX_TEMPLATE);
             BufferedReader br = new BufferedReader(new InputStreamReader(in));
             BufferedWriter bw = new BufferedWriter((new FileWriter(newFile.toFile())))) {
            while (br.ready()) {
                String line = br.readLine();

                line = line.replaceAll("##TITLE##", configData.getTitle());
                line = line.replaceAll("##MAX_LAYERS##", Integer.toString(nrZoomLevels - 1));
                line = line.replaceAll("##IMAGE_FORMAT##", imageFormat.getEnding());
                line = line.replaceAll("##IMAGE_SIZE##", Integer.toString(Config.SIZE_IMAGE));
                line = line.replaceAll("##IMAGE_SIZE_HALF##", Integer.toString(Config.SIZE_IMAGE / 2));
                line = line.replaceAll("##MARKERS##", markerString);
                line = line.replaceAll("##TILELAYERS##", tileLayerString);
                line = line.replaceAll("##TOTAL_PIXEL_SIZE##", "" + (Math.pow(2, nrZoomLevels) * Config.SIZE_IMAGE));
                line = line.replaceAll("##START_LAYER##", "layer" + layers.get(0).getId());

                bw.write(line);
                bw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateMarkerInformations() {
        StringBuffer rc = new StringBuffer();

        generateMapMarkers(rc);
        rc.append("\n\n");
        generateVarOverlay(rc);
        rc.append("\n\n");
        generateLayerAssignment(rc);


        return rc.toString();
    }

    private String generateTileLayerString() {
        float mapBorderX1 = -1 * Config.SIZE_IMAGE;
        float mapBorderY1 = 0;
        float mapBorderX2 = 0;
        float mapBorderY2 = Config.SIZE_IMAGE;

        StringBuffer rc = new StringBuffer();
        rc.append("\n");
        rc.append("// create layers\n");
        for (Layer layer : layers) {
            rc.append("var layer");
            rc.append(layer.getId());
            rc.append(" = L.tileLayer('");
            rc.append(layer.getId());
            rc.append("/tile_{z}_{x}-{y}.jpg', {\n");

            rc.append("	minZoom: 0\n");
            rc.append("	,id : \"").append(layer.getName()).append("\"\n");
            rc.append("	,maxZoom: 6\n");
            rc.append("	,zoomOffset : 0\n");
            rc.append("	,attribution: 'Covenant'\n");
            rc.append("\t,tileSize:").append(Integer.toString(Config.SIZE_IMAGE)).append("\n");
            rc.append("\t,continuousWorld: false\n");
            rc.append("\t,noWrap: true\n");
            rc.append("\t,tms: false\n");
            rc.append("\t,crs: L.CRS.Simple\n");
            rc.append("\t,bounds: [\n");
            rc.append("\t         \t[" + mapBorderX1 + "," + mapBorderY1 + "],\n");
            rc.append("\t         \t[" + mapBorderX2 + "," + mapBorderY2 + "]\n");
            rc.append("\t         ]\n");
            rc.append("});\n");
            rc.append("\n");
        }

        rc.append("\n");
        rc.append("// assign layers\n");

        rc.append("var myLayers = {\n");
        for (Layer layer : layers) {
            rc.append("\t\"");
            rc.append(layer.getName());
            rc.append("\"");
            rc.append(" : layer");
            rc.append(layer.getId());
            rc.append(",");
            rc.append("\n");
        }
        rc.append("}\n");
        rc.append("\n");


        return rc.toString();
    }

    private void generateLayerAssignment(StringBuffer rc) {
        rc.append("L.control.layers(myLayers, overlays).addTo(map);\n");
    }

    private void generateMapMarkers(StringBuffer rc) {
        if (configData.getMarkerEntries() == null) {
            return;
        }

        int groupNr = 0;
        MarkerConverter converter = new MarkerConverter(configData);

        for (MarkerGroup markerGroup : configData.getMarkerEntries().getGroups()) {
            rc.append("var group").append(groupNr++);
            rc.append(" = ");
            rc.append(" L.layerGroup( ");
            rc.append(" [ ");
            for (MarkerEntry markerEntry : markerGroup.getMarkers()) {
                rc.append("\n");
                rc.append(converter.toJavaScript(markerEntry));
                rc.append(",");
            }
            rc.delete(rc.length() - 1, rc.length());
            rc.append("\n");
            rc.append(" ] ");
            rc.append(" ); ");
            rc.append("\n");
        }
    }

    private void generateVarOverlay(StringBuffer rc) {
        if (configData.getMarkerEntries() == null) {
            return;
        }

        int groupNr = 0;
        rc.append("var overlays = {");
        for (MarkerGroup markerGroup : configData.getMarkerEntries().getGroups()) {
            rc.append("\n");
            rc.append("\t");
            rc.append("\"").append(markerGroup.getName()).append("\"");
            rc.append(":");
            rc.append("group").append(groupNr++);
            rc.append(",");
        }
        rc.delete(rc.length() - 1, rc.length());
        rc.append("\n");
        rc.append("};\n");

    }

    public void copyAdditionalFiles(String finalOutputDirectory) {
        try {
            Files.copy(
                    getClass().getResourceAsStream(PATH_SCROLL_IMAGE)
                    , Paths.get(finalOutputDirectory, Config.BASEDIR_IMAGES, "scroll.png")
                    , StandardCopyOption.REPLACE_EXISTING
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * X-Axis
     */
    private float getLongitude(float value) {
        float rc;

        float width = configData.getCc3TopX() - configData.getCc3BottomX();
        float newRange = Config.SIZE_IMAGE;

        rc = (value - configData.getCc3BottomX()) / width * newRange;
        return rc;
    }

    /**
     * Y-Axis
     */
    private float getLatitude(float value) {
        float rc;
        float height = configData.getCc3TopY() - configData.getCc3BottomY();
        float newRange = Config.SIZE_IMAGE;

        rc = (value - configData.getCc3BottomY()) / height * newRange;

        return newRange - rc;
    }
}
