package de.tkunkel.gmaps.slicer;

public class Config {
    public final static int SIZE_IMAGE = 500;
    public static final String BASEDIR_IMAGES = "/img";

    public enum ImageFormat {
        JPEG("JPEG", "jpg"), PNG("PNG", "png");
        private String ending;
        private String name;

        ImageFormat(String name, String ending) {
            this.ending = ending;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String getEnding() {
            return ending;
        }
    }

    public static final float JPG_QUALITY = 0.85f;
}
