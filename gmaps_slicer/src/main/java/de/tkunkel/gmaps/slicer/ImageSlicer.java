package de.tkunkel.gmaps.slicer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class ImageSlicer {

    private ImageScaler imageScaler;

    public ImageSlicer(BufferedImage originalImage) {
        imageScaler = new ImageScaler(originalImage);
    }

    public void saveImages(int nrLayer, Path outputDirectory, Config.ImageFormat imageFormat) throws IOException, InterruptedException {
        // the number of images is doubled each layer, but the top layer only has one image
        int nrImagesPerSide = (int) Math.pow(2, nrLayer);
        BufferedImage scaledImage = imageScaler.getScaledForLayer(nrLayer);

        for (int row = 0; row < nrImagesPerSide; row++) {
            final int finalRow = row;
            saveColumn(nrImagesPerSide, nrLayer, finalRow, scaledImage, outputDirectory.toString(), imageFormat);
        }
    }

    private void saveColumn(int nrImagesPerSide, int nrLayer, int finalRow, BufferedImage scaledImage, String outputDirectory, Config.ImageFormat imageFormat) {
        for (int column = 0; column < nrImagesPerSide; column++) {
            String filename = String.format("tile_%d_%d-%d.%s", nrLayer, column, finalRow, imageFormat.getEnding());
            System.out.println("DEBUG: creating image '" + filename + "'");
            System.out.println("DEBUG: \tsize: " + column * Config.SIZE_IMAGE
                    + "," + finalRow * Config.SIZE_IMAGE
                    + "," + Config.SIZE_IMAGE
                    + "," + Config.SIZE_IMAGE);
            BufferedImage image = scaledImage.getSubimage(column * Config.SIZE_IMAGE
                    , finalRow * Config.SIZE_IMAGE
                    , Config.SIZE_IMAGE
                    , Config.SIZE_IMAGE
            );
            try {
                ImageIO.write(image, imageFormat.getName(), new File(outputDirectory + "/" + filename));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
