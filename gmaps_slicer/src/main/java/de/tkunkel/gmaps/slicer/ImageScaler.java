package de.tkunkel.gmaps.slicer;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageScaler {
    private BufferedImage originalImage;

    public ImageScaler(BufferedImage originalImage) {
        this.originalImage = originalImage;
    }

    /**
     * scales the original image for the needed layer.<br>
     * Uppermost Layer is layer 0;
     */
    public BufferedImage getScaledForLayer(int nrLayer) {
        BufferedImage rc;
        // the number of images is doubled each layer, but the top layer only has one image
        int factor = (int)Math.pow(2,nrLayer);
        rc = getScaledImage(originalImage, factor * Config.SIZE_IMAGE, factor * Config.SIZE_IMAGE);
        return rc;
    }

    /**
     * Resizes an image using a Graphics2D object backed by a BufferedImage.
     *
     * @param srcImg - source image to scale
     * @param w      - desired width
     * @param h      - desired height
     * @return - the new resized image
     */
    public static BufferedImage getScaledImage(Image srcImg, int w, int h) {
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();
        return resizedImg;
    }
}
