package de.tkunkel.gmaps.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType (XmlAccessType.FIELD)
public class Layer {

    @XmlElement
    private String id;
    @XmlElement
    private String name;
    @XmlElement
    private String additionalMacroCommandsBefore;
    @XmlElement
    private String additionalMacroCommandsAfter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdditionalMacroCommandsBefore() {
        return additionalMacroCommandsBefore;
    }

    public void setAdditionalMacroCommandsBefore(String additionalMacroCommandsBefore) {
        this.additionalMacroCommandsBefore = additionalMacroCommandsBefore;
    }

    public String getAdditionalMacroCommandsAfter() {
        return additionalMacroCommandsAfter;
    }

    public void setAdditionalMacroCommandsAfter(String additionalMacroCommandsAfter) {
        this.additionalMacroCommandsAfter = additionalMacroCommandsAfter;
    }
}
