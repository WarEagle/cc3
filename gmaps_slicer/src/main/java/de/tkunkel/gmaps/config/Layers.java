package de.tkunkel.gmaps.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType (XmlAccessType.FIELD)
public class Layers {

    @XmlElement(name = "layer")
    private List<Layer> layers = new ArrayList<>();

    public List<Layer> getLayers() {
        return layers;
    }

    public void setLayers(List<Layer> layers) {
        this.layers = layers;
    }
}
