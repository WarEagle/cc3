package de.tkunkel.gmaps.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType (XmlAccessType.FIELD)
public class MarkerEntries {

    @XmlElement(name = "group")
    private List<MarkerGroup> groups = new ArrayList<>();

    public List<MarkerGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<MarkerGroup> groups) {
        this.groups = groups;
    }
}
