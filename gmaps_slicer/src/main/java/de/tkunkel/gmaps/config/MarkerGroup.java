package de.tkunkel.gmaps.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType (XmlAccessType.FIELD)
public class MarkerGroup {

    @XmlElement(name = "marker")
    private List<MarkerEntry> markers = new ArrayList<>();

    @XmlAttribute
    private String name;

    public List<MarkerEntry> getMarkers() {
        return markers;
    }

    public void setMarkers(List<MarkerEntry> markers) {
        this.markers = markers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
