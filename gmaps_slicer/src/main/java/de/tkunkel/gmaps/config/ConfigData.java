package de.tkunkel.gmaps.config;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigData {

    @XmlElement(required = true)
    private String title;
    @XmlElement(required = true)
    private Integer cc3ImagesPerSideToExported;
    @XmlElement(required = true)
    private Float cc3BottomX;
    @XmlElement(required = true)
    private Float cc3BottomY;
    @XmlElement(required = true)
    private Float cc3TopX;
    @XmlElement(required = true)
    private Float cc3TopY;
    @XmlElement(required = true)
    private String cc3ImageExportDirectory;
    @XmlElement(required = true)
    private String finalOutputDirectory;
    @XmlElement
    private Layers layers;
    @XmlElement
    private MarkerEntries markerEntries;

    public void setCc3ImageExportDirectory(String cc3ImageExportDirectory) {
        this.cc3ImageExportDirectory = cc3ImageExportDirectory;
    }

    public String getCc3ImageExportDirectory() {
        return cc3ImageExportDirectory;
    }

    public Float getCc3TopY() {
        return cc3TopY;
    }

    public void setCc3TopY(Float cc3TopY) {
        this.cc3TopY = cc3TopY;
    }

    public Float getCc3TopX() {
        return cc3TopX;
    }

    public void setCc3TopX(Float cc3TopX) {
        this.cc3TopX = cc3TopX;
    }

    public Float getCc3BottomY() {
        return cc3BottomY;
    }

    public void setCc3BottomY(Float cc3BottomY) {
        this.cc3BottomY = cc3BottomY;
    }

    public Float getCc3BottomX() {
        return cc3BottomX;
    }

    public void setCc3BottomX(Float cc3BottomX) {
        this.cc3BottomX = cc3BottomX;
    }

    public Integer getCc3ImagesPerSideToExported() {
        return cc3ImagesPerSideToExported;
    }

    public void setCc3ImagesPerSideToExported(Integer cc3ImagesPerSideToExported) {
        this.cc3ImagesPerSideToExported = cc3ImagesPerSideToExported;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MarkerEntries getMarkerEntries() {
        return markerEntries;
    }

    public void setMarkerEntries(MarkerEntries markerEntries) {
        this.markerEntries = markerEntries;
    }

    public String getFinalOutputDirectory() {
        return finalOutputDirectory;
    }

    public void setFinalOutputDirectory(String finalOutputDirectory) {
        this.finalOutputDirectory = finalOutputDirectory;
    }

    public static ConfigData createFromArgs(String[] args) throws JAXBException {
        if (args == null || args.length < 1) {
            System.err.println("usage: configurationFile");
            System.exit(-1);
        }
        File file = new File(args[0]);
        JAXBContext jaxbContext = JAXBContext.newInstance(ConfigData.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ConfigData configData = (ConfigData) unmarshaller.unmarshal(file);
        return configData;
    }

    public Layers getLayers() {
        return layers;
    }

    public void setLayers(Layers layers) {
        this.layers = layers;
    }
}
