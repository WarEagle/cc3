package de.tkunkel.gmaps.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType (XmlAccessType.FIELD)
public class MarkerEntry {
    @XmlElement
    private String title;
    @XmlElement
    private String description;
    @XmlElement
    private Float x;
    @XmlElement
    private Float y;

    public MarkerEntry() {
    }

    public MarkerEntry(String title, Float x, Float y) {
        this.title = title;
        this.x = x;
        this.y= y;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
