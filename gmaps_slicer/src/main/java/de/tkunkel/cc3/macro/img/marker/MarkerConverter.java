package de.tkunkel.cc3.macro.img.marker;

import de.tkunkel.gmaps.config.ConfigData;
import de.tkunkel.gmaps.config.MarkerEntry;
import de.tkunkel.gmaps.slicer.Config;

public class MarkerConverter {
    private ConfigData configData;

    public MarkerConverter(ConfigData configData) {
        this.configData = configData;
    }

    /**
     * X-Axis
     */
    public float getLongitude(MarkerEntry marker) {
        float rc;

        float width = configData.getCc3TopX() - configData.getCc3BottomX();
        float newRange = Config.SIZE_IMAGE;

        rc = (marker.getX() - configData.getCc3BottomX()) / width * newRange;
        return rc;
    }

    /**
     * Y-Axis
     */
    public float getLatitude(MarkerEntry marker) {
        float rc;
        float height = configData.getCc3TopY() - configData.getCc3BottomY();
        float newRange = Config.SIZE_IMAGE;

        rc = (marker.getY() - configData.getCc3BottomY()) / height * newRange;

        return newRange - rc;
    }

    public String toJavaScript(MarkerEntry marker) {
        //denver    = L.marker([39.74, -104.99]).bindPopup('This is Denver, CO.'),
        StringBuilder rc = new StringBuilder();

        rc.append("\t\tL.marker(");
        rc.append("[-");
        rc.append(getLatitude(marker));
        rc.append(",");
        rc.append(getLongitude(marker));
        rc.append("]");
        rc.append(",");
        rc.append("{");
        rc.append("title:");
        rc.append("'");
        rc.append(marker.getTitle());
        rc.append("'");
        rc.append("}");
        rc.append(").bindPopup(");
        rc.append("'");
        rc.append("<div id=\"borderimg\">");
        rc.append(generatePopup(marker));
        rc.append("</div>");
        rc.append("'");

        rc.append(",");
        rc.append("{closeButton: false, offset: [0,0]}");

        rc.append(")");

        return rc.toString();
    }

    private String generatePopup(MarkerEntry marker) {
        StringBuilder rc = new StringBuilder();
        rc.append("<div>");
        rc.append("<b>");
        rc.append(marker.getTitle());
        rc.append("</b>");
        rc.append("</div>");
        if (marker.getDescription() != null) {
            rc.append("<div>");
            rc.append(marker.getDescription());
            rc.append("</div>");
        }
        return rc.toString().replaceAll("\n", "<br>");
    }
}
