package de.tkunkel.cc3.macro;

import de.tkunkel.gmaps.config.ConfigData;
import de.tkunkel.gmaps.config.Layer;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Locale;

public class CC3SlicerMacroGeneratorBatch {
    public static void main(String[] args) throws JAXBException {
        ConfigData configData = ConfigData.createFromArgs(args);

        float xStart = configData.getCc3BottomX();
        float yStart = configData.getCc3BottomY();
        float xEnd = configData.getCc3TopX();
        float yEnd = configData.getCc3TopY();
        int slicesPerSide = configData.getCc3ImagesPerSideToExported();
        String outputDirectory = configData.getCc3ImageExportDirectory();
        new File(outputDirectory).mkdirs();

        float sliceHeigth = Math.abs((yStart - yEnd) / slicesPerSide) + 1;
        float sliceWidth = Math.abs((xStart - xEnd) / slicesPerSide) + 1;

        // check if the area was a rectangle, otherwise abort.
        if (sliceHeigth != sliceWidth) {
            System.err.println("You have to provide a square selection. Currently you gave coordinates to an area of " + (xStart - xEnd) + " x " + (yStart - yEnd));
            System.exit(-1);
        }

        // use the bigger of both, so a rectangular result is generated
        float sliceSize = Math.max(sliceHeigth, sliceWidth);

        for (Layer layer : configData.getLayers().getLayers()) {
            Locale.setDefault(new Locale("en", "US"));
            DecimalFormat myFloatFormatter = new DecimalFormat("#.##");

            int cntMacro = 0;

            for (int cntX = 0; cntX < slicesPerSide; cntX++) {
                for (int cntY = 0; cntY < slicesPerSide; cntY++) {
                    float tmpX = cntX * sliceSize + xStart;
                    float tmpY = cntY * sliceSize + yStart;
                    //TODO add parameters to create boxes on own layer
                    //TODO add parameters to set border and fill of test-boxes
                    //TODO experiment with waiting after export-command to prevent crash in cc3, it crashes too often
                    //TODO Change URL in template_index.html to a local file and copy it during generation
                    //String command = String.format("BOX %s,%s %s,%s", myFloatFormatter.format(tmpX), myFloatFormatter.format(tmpY), myFloatFormatter.format(tmpX + sliceSize), myFloatFormatter.format(tmpY + sliceSize));
                    String command = String.format("WBSM %s" + "/cc3export_x%02d-y%02d.%s\n%s,%s\n%s,%s", outputDirectory, cntX, cntY, "png", myFloatFormatter.format(tmpX), myFloatFormatter.format(tmpY), myFloatFormatter.format(tmpX + sliceSize), myFloatFormatter.format(tmpY + sliceSize));
                    System.out.println("MACRO MySlice" + layer.getId() + "_" + cntMacro++);
                    System.out.print(simpleLines(layer.getAdditionalMacroCommandsBefore()));
                    System.out.println("PAUSE 2");
                    System.out.println(command);
                    System.out.println("PAUSE 2");
                    System.out.print(simpleLines(layer.getAdditionalMacroCommandsAfter()));
                    System.out.println("SAVE");
                    System.out.println("PAUSE 2");
                    System.out.println("EXIT");
                    System.out.println("ENDM");
                    System.out.println();
                }
            }
            System.out.println("MACRO MySlice");
            for (int i = 0; i < cntMacro; i++) {
                System.out.println("MySlice" + i);
            }
            System.out.println("ENDM");
        }

    }

    private static String simpleLines(String additionalMacroCommandsBefore) {
        if (additionalMacroCommandsBefore == null) {
            return "";
        }

        StringBuilder rc = new StringBuilder();
        for (String line : additionalMacroCommandsBefore.split("\n")) {
            if (line.trim().length() < 1) {
                continue;
            }
            rc.append(line.trim());
            rc.append("\n");
        }
        return rc.toString();
    }

}
