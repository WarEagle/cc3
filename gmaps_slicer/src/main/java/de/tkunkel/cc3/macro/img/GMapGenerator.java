package de.tkunkel.cc3.macro.img;

import de.tkunkel.gmaps.config.ConfigData;
import de.tkunkel.gmaps.config.Layer;
import de.tkunkel.gmaps.slicer.Config;
import de.tkunkel.gmaps.slicer.HtmlUpdater;
import de.tkunkel.gmaps.slicer.ImageScaler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.xml.bind.JAXBException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class GMapGenerator {
    private static Logger LOG = LogManager.getLogger(GMapGenerator.class);

    private final static String inputFilenamePattern = "cc3export_x%02d-y%02d.%s";
    private final static String outputFilenamePattern = "tile_%d_%d-%d.%s";
    private final static Config.ImageFormat imageFormatInput = Config.ImageFormat.PNG;
    private final static Config.ImageFormat imageFormatOutput = Config.ImageFormat.JPEG;

    public GMapGenerator() {
    }

    public static void main(String[] args) throws IOException, InterruptedException, JAXBException {
        boolean generateOnlyHtmlPage = false;
        ConfigData configData = ConfigData.createFromArgs(args);

        if (args.length > 1) {
            if (args[1].toLowerCase().equals("-onlyhtml")) {
                LOG.info("Generating only HTML!");
                generateOnlyHtmlPage = true;
            }
        }

        GMapGenerator generator = new GMapGenerator();
        //because all layers are the same, we can use the first layer for the sizes
        String inputDirectory = configData.getCc3ImageExportDirectory() + '/' + configData.getLayers().getLayers().get(0).getId();
        int maxZoomLevel = generator.getHighestZoomLevel(inputDirectory, configData.getCc3ImagesPerSideToExported());
        if (!generateOnlyHtmlPage){
            for (Layer layer : configData.getLayers().getLayers()) {
                String LayerOutputDirectoryImages = configData.getFinalOutputDirectory() + '/' + layer.getId();
                String layerInputDirectory = configData.getCc3ImageExportDirectory() + '/' + layer.getId();
                generator.createDirectory(LayerOutputDirectoryImages);
                generator.generateImages(layerInputDirectory, LayerOutputDirectoryImages, configData.getCc3ImagesPerSideToExported(), maxZoomLevel);
            }
        }
        //int maxZoomLevel = 2;
        HtmlUpdater updater = new HtmlUpdater(configData, maxZoomLevel + 1, imageFormatOutput, configData.getLayers().getLayers());
        updater.writeToNewFile(Paths.get(configData.getFinalOutputDirectory(), "index.html"));
        generator.createDirectory(configData.getFinalOutputDirectory() + '/' + Config.BASEDIR_IMAGES);
        updater.copyAdditionalFiles(configData.getFinalOutputDirectory());

        LOG.info("done.");
    }

    private void generateImages(String inputDirectory, String outputDirectoryImages, int cc3SlicesPerSide, int maxZoomLevel) throws IOException, InterruptedException {

        createDirectory(outputDirectoryImages);
        generateImagesForHighestZoom(inputDirectory, outputDirectoryImages, cc3SlicesPerSide);
        for (int currentZoomLevel = maxZoomLevel - 1; currentZoomLevel >= 0; currentZoomLevel--) {
            generateImagesForZoomlevel(currentZoomLevel, outputDirectoryImages);
        }
    }

    /**
     * this method must be called from bottom to up, so the biggest zoomed images must exist, those will be aggregated to a lower zoom level.
     */
    private void generateImagesForZoomlevel(int currentZoomLevel, String outputDirectoryImages) throws IOException, InterruptedException {
        ExecutorService executor = createExecutor();
        LOG.debug("generating for currentZoomLevel=" + currentZoomLevel);
        int imagesPerSide = (int) Math.pow(2, currentZoomLevel);
        LOG.debug("generating images per side =" + imagesPerSide);


        for (int imageCoordinateX = 0; imageCoordinateX < imagesPerSide; imageCoordinateX++) {
            for (int imageCoordinateY = 0; imageCoordinateY < imagesPerSide; imageCoordinateY++) {
                final int finalImageCoordinateX = imageCoordinateX;
                final int finalImageCoordinateY = imageCoordinateY;
                executor.submit(() -> {
                    try {
                        generateSingleImageForZoomLevel(currentZoomLevel, outputDirectoryImages, finalImageCoordinateX, finalImageCoordinateY);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                });
            }
        }
        executor.shutdown();
        executor.awaitTermination(5, TimeUnit.MINUTES);

    }

    private void generateSingleImageForZoomLevel(int currentZoomLevel, String outputDirectoryImages, int imageCoordinateX, int imageCoordinateY) throws IOException {
        String filenameOutput = String.format(outputFilenamePattern, currentZoomLevel, imageCoordinateX, imageCoordinateY, imageFormatOutput.getEnding());
        LOG.debug("writing = " + filenameOutput);
        BufferedImage newImage = new BufferedImage(Config.SIZE_IMAGE * 2, Config.SIZE_IMAGE * 2, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = newImage.createGraphics();

        int usageX = (imageCoordinateX) * 2;
        int usageY = (imageCoordinateY) * 2;
        String filenameInput1 = String.format(outputFilenamePattern, currentZoomLevel + 1, usageX, usageY, imageFormatOutput.getEnding());
        String filenameInput2 = String.format(outputFilenamePattern, currentZoomLevel + 1, usageX, usageY + 1, imageFormatOutput.getEnding());
        String filenameInput3 = String.format(outputFilenamePattern, currentZoomLevel + 1, usageX + 1, usageY, imageFormatOutput.getEnding());
        String filenameInput4 = String.format(outputFilenamePattern, currentZoomLevel + 1, usageX + 1, usageY + 1, imageFormatOutput.getEnding());
        LOG.debug("\tusing " + filenameInput1 + " as input.");
        LOG.debug("\tusing " + filenameInput2 + " as input.");
        LOG.debug("\tusing " + filenameInput3 + " as input.");
        LOG.debug("\tusing " + filenameInput4 + " as input.");

        BufferedImage imageInput1 = getImage(outputDirectoryImages, filenameInput1);
        BufferedImage imageInput2 = getImage(outputDirectoryImages, filenameInput2);
        BufferedImage imageInput3 = getImage(outputDirectoryImages, filenameInput3);
        BufferedImage imageInput4 = getImage(outputDirectoryImages, filenameInput4);

        g.drawImage(imageInput1, 0, 0, Config.SIZE_IMAGE, Config.SIZE_IMAGE, null);
        g.drawImage(imageInput2, 0, Config.SIZE_IMAGE, Config.SIZE_IMAGE, Config.SIZE_IMAGE, null);
        g.drawImage(imageInput3, Config.SIZE_IMAGE, 0, Config.SIZE_IMAGE, Config.SIZE_IMAGE, null);
        g.drawImage(imageInput4, Config.SIZE_IMAGE, Config.SIZE_IMAGE, Config.SIZE_IMAGE, Config.SIZE_IMAGE, null);

        g.dispose();

        BufferedImage scaledImage = ImageScaler.getScaledImage(newImage, Config.SIZE_IMAGE, Config.SIZE_IMAGE);
        imageInput1 = null;
        imageInput2 = null;
        imageInput3 = null;
        imageInput4 = null;
        newImage = null;

        saveImage(scaledImage, outputDirectoryImages, filenameOutput);
    }

    private void saveImage(BufferedImage scaledImage, String outputDirectoryImages, String filenameOutput) throws IOException {
        ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName(imageFormatOutput.getName()).next();
        ImageWriteParam jpgWriteParam = jpgWriter.getDefaultWriteParam();
        jpgWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        jpgWriteParam.setCompressionQuality(Config.JPG_QUALITY);

        ImageOutputStream outputStream = new FileImageOutputStream(Paths.get(outputDirectoryImages, filenameOutput).toFile());
        jpgWriter.setOutput(outputStream);
        IIOImage outputImage = new IIOImage(scaledImage, null, null);
        jpgWriter.write(null, outputImage, jpgWriteParam);
        jpgWriter.dispose();
    }

    private void createDirectory(String outputDirectory) {
        File dir = new File(outputDirectory);
        if (dir.exists() && dir.isDirectory()) {
            return;
        }

        boolean dirsCreated = dir.mkdirs();
        if (!dirsCreated) {
            LOG.error("Could not create outputdirectory " + outputDirectory);
            System.exit(-1);
        }
    }

    private int getHighestZoomLevel(String inputDirectory, int cc3SlicesPerSide) throws IOException, InterruptedException {
        int imageWidth = getImageWidth(Paths.get(inputDirectory, String.format(inputFilenamePattern, 0, 0, imageFormatInput.getEnding())));

        if (imageWidth % Config.SIZE_IMAGE != 0) {
            System.err.println("width isn't a multiple of " + Config.SIZE_IMAGE + " it is " + imageWidth);
            System.exit(-1);
        }
        int sourceSubImagesForWidth = imageWidth / Config.SIZE_IMAGE;
        int zoomLevel = logRoundedUp((sourceSubImagesForWidth * cc3SlicesPerSide), 2);
        return zoomLevel;
    }

    private void generateImagesForHighestZoom(String inputDirectory, String outputDirectory, int cc3SlicesPerSide) throws IOException, InterruptedException {

        ExecutorService executor = createExecutor();
        int imageWidth = getImageWidth(Paths.get(inputDirectory, String.format(inputFilenamePattern, 0, 0, imageFormatInput.getEnding())));
        int imageHeight = getImageHeight(Paths.get(inputDirectory, String.format(inputFilenamePattern, 0, 0, imageFormatInput.getEnding())));

        if (imageWidth % Config.SIZE_IMAGE != 0) {
            System.err.println("width isn't a multiple of " + Config.SIZE_IMAGE + " it is " + imageWidth);
            System.exit(-1);
        }
        if (imageHeight % Config.SIZE_IMAGE != 0) {
            System.err.println("height isn't a multiple of " + Config.SIZE_IMAGE + " it is " + imageHeight);
            System.exit(-1);
        }

        int sourceSubImagesForWidth = imageWidth / Config.SIZE_IMAGE;
        int sourceSubImagesForHeight = imageHeight / Config.SIZE_IMAGE;
        int zoomLevel = getHighestZoomLevel(inputDirectory, cc3SlicesPerSide);
        LOG.debug("source width = " + imageWidth);
        LOG.debug("target width = " + Config.SIZE_IMAGE);
        LOG.debug("source height = " + imageHeight);
        LOG.debug("target height = " + Config.SIZE_IMAGE);
        LOG.debug("calculated max zoomlevel = " + zoomLevel);
        LOG.debug("sourceSubImagesForWidth = " + sourceSubImagesForWidth);
        //System.exit(-1);

        for (int currentSourceColumn = 0; currentSourceColumn < cc3SlicesPerSide; currentSourceColumn++) {
            for (int currentSourceRow = 0; currentSourceRow < cc3SlicesPerSide; currentSourceRow++) {
                String filenameInput = String.format(inputFilenamePattern, currentSourceColumn, cc3SlicesPerSide - currentSourceRow - 1, imageFormatInput.getEnding());
                BufferedImage imageInput = getImage(inputDirectory, filenameInput);
                LOG.info("reading = " + filenameInput);
                final int finalCurrentSourceColumn = currentSourceColumn;
                final int finalCurrentSourceRow = currentSourceRow;
                executor.submit(() -> {
                    try {
                        sliceImage(outputDirectory, cc3SlicesPerSide, sourceSubImagesForWidth, sourceSubImagesForHeight, finalCurrentSourceColumn, finalCurrentSourceRow, imageInput, zoomLevel);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                });
            }
        }
        executor.shutdown();
        executor.awaitTermination(5, TimeUnit.MINUTES);

        generateMissingImages(zoomLevel, sourceSubImagesForWidth * cc3SlicesPerSide, sourceSubImagesForHeight * cc3SlicesPerSide, outputDirectory);
    }

    private BufferedImage getImage(String inputDirectory, String filenameInput) throws IOException {
        try {
            return ImageIO.read(Paths.get(inputDirectory, filenameInput).toFile());
        } catch (Exception ex) {
            LOG.error(inputDirectory);
            LOG.error(filenameInput);
            LOG.error("", ex);
            System.exit(-1);
        }
        return null;
    }

    /**
     * generates the missing images, they are needed so the zoom level is square and zoomable
     */
    private void generateMissingImages(int zoomLevel, int imagesWidth, int imagesHeight, String outputDirectory) throws IOException, InterruptedException {
        ExecutorService executor = createExecutor();
        BufferedImage newImage = new BufferedImage(Config.SIZE_IMAGE, Config.SIZE_IMAGE, BufferedImage.TYPE_INT_RGB);

        int numberOfNeededImagesPerSide = (int) Math.pow(2, zoomLevel);
        LOG.debug("zoomLevel=" + zoomLevel);
        LOG.debug("numberOfNeededImagesPerSide=" + numberOfNeededImagesPerSide);
        for (int x = 0; x < numberOfNeededImagesPerSide; x++) {
            final int finalX = x;
            executor.submit(() -> {
                for (int y = 0; y < numberOfNeededImagesPerSide; y++) {
                    if (finalX < imagesWidth && y < imagesHeight) {
                        // this image was already generated as "real" image, no need to generate a missing image for those coordinates.
                        continue;
                    }
                    String filenameOutput = String.format(outputFilenamePattern, zoomLevel, finalX, y, imageFormatOutput.getEnding());
                    LOG.info("writing missing image = " + filenameOutput);
                    try {
                        ImageIO.write(newImage, imageFormatOutput.getName(), Paths.get(outputDirectory, filenameOutput).toFile());
                        newImage.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        executor.shutdown();
        executor.awaitTermination(5, TimeUnit.MINUTES);
    }

    private void sliceImage(String outputDirectory, int slicesPerSide, int sourceSubImagesForWidth, int sourceSubImagesForHeight, int currentSourceColumn, int currentSourceRow, BufferedImage imageInput, int zoomLevel) throws IOException {
        LOG.debug("slicesPerSide = " + slicesPerSide);
        LOG.debug("currentSourceColumn = " + currentSourceColumn);
        LOG.debug("currentSourceRow = " + currentSourceRow);
        LOG.debug("sourceSubImagesForWidth = " + sourceSubImagesForWidth);
        LOG.debug("sourceSubImagesForHeight = " + sourceSubImagesForHeight);
        for (int x = 0; x < sourceSubImagesForWidth; x++) {
            for (int y = 0; y < sourceSubImagesForHeight; y++) {
                int imageCoordinateX = currentSourceColumn * sourceSubImagesForWidth + x;
                int imageCoordinateY = currentSourceRow * sourceSubImagesForHeight + y;
                int startX = x * Config.SIZE_IMAGE;
                int startY = y * Config.SIZE_IMAGE;

                BufferedImage newImage = new BufferedImage(Config.SIZE_IMAGE, Config.SIZE_IMAGE, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = newImage.createGraphics();
                Image subImage = imageInput.getSubimage(startX, startY, Config.SIZE_IMAGE, Config.SIZE_IMAGE);
                g.drawImage(subImage, 0, 0, Config.SIZE_IMAGE, Config.SIZE_IMAGE, null);

                String filenameOutput = String.format(outputFilenamePattern, zoomLevel, imageCoordinateX, imageCoordinateY, imageFormatOutput.getEnding());
                LOG.info("writing = " + filenameOutput);
                ImageIO.write(newImage, imageFormatOutput.getName(), Paths.get(outputDirectory, filenameOutput).toFile());
                newImage.flush();
            }
        }
    }

    private int getImageWidth(Path imageFile) throws IOException {
        int rc = -1;
        try {
            BufferedImage image = ImageIO.read(imageFile.toFile());
            rc = image.getWidth();
        } catch (IOException e) {
            System.err.println(imageFile);
            e.printStackTrace(System.err);
            System.exit(-1);
        }
        return rc;
    }

    private int getImageHeight(Path imageFile) throws IOException {
        int rc = -1;
        try {
            BufferedImage image = ImageIO.read(imageFile.toFile());
            rc = image.getHeight();
        } catch (IOException e) {
            System.err.println(imageFile);
            e.printStackTrace(System.err);
            System.exit(-1);
        }

        return rc;
    }

    int logRoundedUp(int x, int base) {
        return (int) Math.ceil(Math.log(x) / Math.log(base));
    }

    private ExecutorService createExecutor() {
        return Executors.newFixedThreadPool(8);
//        return Executors.newSingleThreadExecutor();
//        return Executors.newCachedThreadPool();
    }

}
