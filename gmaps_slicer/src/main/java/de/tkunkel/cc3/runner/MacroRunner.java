package de.tkunkel.cc3.runner;

import org.apache.logging.log4j.LogManager;

import java.awt.*;
import java.util.Set;
import java.util.TreeSet;

/**
 * Starts CC3 and runs a macro, then after CC3 is finished, starts the next macro
 */
public class MacroRunner {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger(MacroRunner.class);

    private Set<String> macros = new TreeSet<>();
//    private String commandCC3 = "C:\\Program Files (x86)\\ProFantasy\\CC3Plus\\fcw32.exe";
    private String commandCC3 = "notepad.exe";
    private long sleepUntilCC3IsStarted = 3000;
    private Robot robot;
    private Process exec;

    public MacroRunner() throws AWTException {
        robot = new Robot();
    }

    public static void main(String[] args) throws AWTException {
        MacroRunner runner = new MacroRunner();
        Set<String> macros = new TreeSet<>();
        for (int i = 0; i < 9; i++) {
            macros.add("MySlice"+i);
        }

        runner.setMacros(macros);
        runner.executeMacros();
    }

    private void waitForCC3Ended() {
        try {
            exec.waitFor();
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    private void executeMacros() {
        macros.forEach((macro) -> {
            executeMacro(macro);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                LOG.error(e);
            }
        });
    }

    private void executeMacro(String macro) {
        LOG.debug("executing macro: " + macro);

        startCC3();
        type(macro);
        type("\n");
        waitForCC3Ended();
    }

    public void setMacros(Set<String> macros) {
        this.macros = macros;
    }

    private void startCC3() {
        try {
            exec = Runtime.getRuntime().exec(commandCC3);
            Thread.sleep(sleepUntilCC3IsStarted);
            EnumerateWindows.bringWindowByClassnameToFront("FCW32");
        } catch (Exception e) {
            LOG.error(e);
            System.exit(-1);
        }
    }

    private void type(String s) {
        byte[] bytes = s.getBytes();
        for (byte b : bytes) {
            int code = b;
            // keycode only handles [A-Z] (which is ASCII decimal [65-90])
            if (code > 96 && code < 123) code = code - 32;
            robot.delay(40);
            robot.keyPress(code);
            robot.keyRelease(code);
        }
    }
}
