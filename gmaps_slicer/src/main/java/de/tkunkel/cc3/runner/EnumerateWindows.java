package de.tkunkel.cc3.runner;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import java.awt.*;

public class EnumerateWindows {

    private EnumerateWindows() {
        throw new UnsupportedOperationException();
    }

    public static void bringWindowByClassnameToFront(final String windowName) throws Exception {
        Point rcPoint = null;

        HWND hwnd = User32.INSTANCE.FindWindow(windowName, null);

        if (hwnd != null) {
            User32 user32 = User32.INSTANCE;
            user32.ShowWindow(hwnd, User32.SW_SHOW);
            user32.SetForegroundWindow(hwnd);
        } else {
            throw new Exception("Cannot find window " + windowName);
        }
    }

    private interface User32 extends StdCallLibrary {
        User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class, W32APIOptions.DEFAULT_OPTIONS);

        HWND FindWindow(String lpClassName, String lpWindowName);

        boolean ShowWindow(HWND hWnd, int nCmdShow);

        boolean SetForegroundWindow(HWND hWnd);

        int SW_SHOW = 1;

        int GetClientRect(HWND handle, int[] rect);

        int ClientToScreen(HWND handle, int[] rect);
    }

    public static void main(String[] args) throws Exception {
        EnumerateWindows.bringWindowByClassnameToFront("FCW32");
    }

}