package de.tkunkel.gmaps.config;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class TestConfigData {
    public static void main(String[] args) {

        ConfigData configData = new ConfigData();
        configData.setCc3BottomX(-2805.0f);
        configData.setCc3BottomY(-2430.0f);
        configData.setCc3TopX(4894.0f);
        configData.setCc3TopY(3027.0f);
        configData.setCc3ImagesPerSideToExported(3);
        configData.setCc3ImageExportDirectory("c:\\Users\\WarEagle\\Desktop\\1\\");
        configData.setFinalOutputDirectory("C:/Users/WarEagle/Desktop/1/land");
        configData.setMarkerEntries(new MarkerEntries());

        MarkerGroup group1 = new MarkerGroup();
        group1.setName("Group 1");
        group1.getMarkers().add(new MarkerEntry("Marker 1-1", 1f, 1f));
        group1.getMarkers().add(new MarkerEntry("Marker 1-2", 2f, 2f));

        MarkerGroup group2 = new MarkerGroup();
        group2.setName("Group 2");
        group2.getMarkers().add(new MarkerEntry("Marker 2-1", 1f, 1f));
        group2.getMarkers().add(new MarkerEntry("Marker 2-2", 2f, 2f));
        group2.getMarkers().add(new MarkerEntry("Marker 2-3", 3f, 3f));

        configData.getMarkerEntries().getGroups().add(group1);
        configData.getMarkerEntries().getGroups().add(group2);


        try {

            //File file = new File("e:\\file.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(ConfigData.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            //jaxbMarshaller.marshal(configData, file);
            jaxbMarshaller.marshal(configData, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }}
