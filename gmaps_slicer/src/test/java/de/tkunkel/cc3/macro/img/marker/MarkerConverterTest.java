package de.tkunkel.cc3.macro.img.marker;

import de.tkunkel.gmaps.config.ConfigData;
import de.tkunkel.gmaps.config.MarkerEntry;
import org.junit.Assert;
import org.junit.Test;

public class MarkerConverterTest {

    @Test
    public void testConverterCenter() {
        ConfigData configData = new ConfigData();
        configData.setCc3BottomX(-10f);
        configData.setCc3BottomY(-10f);
        configData.setCc3TopX(10f);
        configData.setCc3TopY(10f);

        MarkerEntry marker1 = new MarkerEntry("", 0f, 0f);

        MarkerConverter converter = new MarkerConverter(configData);
        Assert.assertEquals(0f, converter.getLongitude(marker1), 0);
        Assert.assertEquals(0f, converter.getLatitude(marker1), 0);
    }

    @Test
    public void testConverterTopLeft() {
        ConfigData configData = new ConfigData();
        configData.setCc3BottomX(-10f);
        configData.setCc3BottomY(-10f);
        configData.setCc3TopX(10f);
        configData.setCc3TopY(10f);

        MarkerEntry marker1 = new MarkerEntry("", -10f, 10f);

        MarkerConverter converter = new MarkerConverter(configData);
        Assert.assertEquals(-180f, converter.getLongitude(marker1), 0);
        Assert.assertEquals(90f, converter.getLatitude(marker1), 0);
    }

    @Test
    public void testConverterBottomRight() {
        ConfigData configData = new ConfigData();
        configData.setCc3BottomX(-10f);
        configData.setCc3BottomY(-10f);
        configData.setCc3TopX(10f);
        configData.setCc3TopY(10f);

        MarkerEntry marker1 = new MarkerEntry("", 10f, -10f);

        MarkerConverter converter = new MarkerConverter(configData);
        Assert.assertEquals(180f, converter.getLongitude(marker1), 0);
        Assert.assertEquals(-90f, converter.getLatitude(marker1), 0);
    }

    @Test
    public void testConverterSomwhereBottomRight() {
        ConfigData configData = new ConfigData();
        configData.setCc3BottomX(-10f);
        configData.setCc3BottomY(-10f);
        configData.setCc3TopX(10f);
        configData.setCc3TopY(10f);

        MarkerEntry marker1 = new MarkerEntry("", 5f, -5f);

        MarkerConverter converter = new MarkerConverter(configData);
        Assert.assertEquals(90f, converter.getLongitude(marker1), 0);
        Assert.assertEquals(-45f, converter.getLatitude(marker1), 0);
    }

    @Test
    public void testConverter1() {
        ConfigData configData = new ConfigData();
        configData.setCc3BottomX(-3.0f);
        configData.setCc3BottomY(-50f);
        configData.setCc3TopX(310f);
        configData.setCc3TopY(250f);

        MarkerEntry marker1 = new MarkerEntry("", 0f, 0f);

        MarkerConverter converter = new MarkerConverter(configData);
        Assert.assertEquals(-176f, converter.getLongitude(marker1), 1);
        Assert.assertEquals(-60f, converter.getLatitude(marker1), 0);
    }

    @Test
    public void testConverterJavascript() {
        ConfigData configData = new ConfigData();
        configData.setCc3BottomX(-10f);
        configData.setCc3BottomY(-10f);
        configData.setCc3TopX(10f);
        configData.setCc3TopY(10f);

        MarkerEntry marker1 = new MarkerEntry("Dummy", 10f, -10f);

        MarkerConverter converter = new MarkerConverter(configData);
        Assert.assertEquals("L.marker([-90.0,180.0]).bindPopup('Dummy')", converter.toJavaScript(marker1));
    }
}